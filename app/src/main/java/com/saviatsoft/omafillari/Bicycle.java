package com.saviatsoft.omafillari;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.auth.FirebaseUser;

public class Bicycle implements Parcelable {

    private String brand, model, picture, serial, owner, id;
    private Boolean stolen;


    public Bicycle(){
        this.brand = "";
        this.model = "";
        this.picture = "";
        this.serial = "";
        this.owner = "";
        this.stolen = false;
        this.id = "";
    }

    protected Bicycle(Parcel in) {
        brand = in.readString();
        model = in.readString();
        picture = in.readString();
        serial = in.readString();
        owner = in.readString();
        id = in.readString();
        byte tmpStolen = in.readByte();
        stolen = tmpStolen == 0 ? null : tmpStolen == 1;
    }

    public static final Creator<Bicycle> CREATOR = new Creator<Bicycle>() {
        @Override
        public Bicycle createFromParcel(Parcel in) {
            return new Bicycle(in);
        }

        @Override
        public Bicycle[] newArray(int size) {
            return new Bicycle[size];
        }
    };

    //setterit
    public void setBrand(String _brand){
        this.brand = _brand;
    }

    //setterit
    public void setId(String _id){
        this.id = _id;
    }

    public void setModel(String _model){
        this.model = _model;
    }

    public void setPicture(String _picture){
        this.picture = _picture;
    }

    public void setSerial(String _serial){
        this.serial = _serial;
    }

    public void setStolen(Boolean _stolen){
        this.stolen = _stolen;
    }

    public void setOwnerByUser(FirebaseUser _user){
        this.owner = _user.getUid();
    }

    public void setOwnerByString(String _user){
        this.owner = _user;
    }

    //getterit
    public String getBrand(){
        return this.brand;
    }

    public String getModel(){
        return this.model;
    }

    public String getPicture(){
        return this.picture;
    }

    public String getSerial(){
        return this.serial;
    }

    public Boolean getStolen(){
        return this.stolen;
    }

    public String getOwner(){
        return this.owner;
    }

    public String getId(){
        return this.id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(brand);
        parcel.writeString(model);
        parcel.writeString(picture);
        parcel.writeString(serial);
        parcel.writeString(owner);
        parcel.writeString(id);
        parcel.writeByte((byte) (stolen == null ? 0 : stolen ? 1 : 2));
    }
}
