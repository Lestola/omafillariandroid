package com.saviatsoft.omafillari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

public class BicycleActivity extends AppCompatActivity implements OnFailureListener{

    private static final String TAG = "MARKO-BICYCLE-ACTIVITY";

    //firebase kamaa
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseFirestore db;
    StorageReference mStorageRef;

    //kameran pyyntö koodi ja kameran kuvan uri
    private static final int CAMERA_REQUEST_CODE = 44;
    Uri uri;

    //UI elements
    private TextView tvSerial, tvBrand, tvModel, tvOwner, tvAddress, tvPhone, tvStolen;
    private Switch sStolen;
    private ImageView ivPhoto;
    private CardView cvPersonalInfoBox, cvStolenInfoBox, cvPhotoOfBike;

    //latauksen etenemis indikaattori
    private ProgressDialog mProgress;

    //pyörän tiedot
    private Boolean ownBike;
    private String bicycleIDnumber;
    private String bicyclePictureFilename;
    private GeoPoint lastLocation;

    private Bicycle activeBicycle = new Bicycle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bicycle);
        //logitus
        Log.d(TAG, "Avattiin oma pyörä aktiviteetti");

        //määritellään database
        db = FirebaseFirestore.getInstance();

        //pyörien latauksen progress indikaattorin määrittely
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Haetaan polkupyörän kuvaa..");

        //tehdään back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //annetaan firebase storagen referenssi
        mStorageRef = FirebaseStorage.getInstance().getReference();

        //määritellään viewit
        tvSerial = findViewById(R.id.bicycle_tvSerial);
        tvBrand = findViewById(R.id.bicycle_tvBrand);
        tvModel = findViewById(R.id.bicycle_tvModel);
        tvOwner = findViewById(R.id.bicycle_tvOwnerName);
        tvAddress = findViewById(R.id.bicycle_tvAddress);
        tvPhone = findViewById(R.id.bicycle_tvPhoneNumber);
        sStolen = findViewById(R.id.bicycle_sStolen);
        tvStolen = findViewById(R.id.tvStolenText);
        ivPhoto = findViewById(R.id.bicycle_ivPhoto);
        cvPersonalInfoBox = findViewById(R.id.cvPersonInfoBox);
        cvStolenInfoBox = findViewById(R.id.cvStolenInfoBox);
        cvPhotoOfBike = findViewById(R.id.cvPhotoOfBike);

        //haetaan edellisen näytön lähettämät tiedot bundlesta,
        //eli onko pyörä oma ja Bicycle objekti
        Bundle bundle = getIntent().getExtras();
        ownBike = bundle.getBoolean("ownBike");

        activeBicycle = getIntent().getParcelableExtra("bicycleObject");
        Log.d(TAG,"Pyörän haku seriaalisaatiosta onnistui:" + activeBicycle.getId() + " <- todisteena pyörän id");

        //päivitetään tekstikentät
        updateScreenFields(activeBicycle);

        //päivitetään pyörän kuva
        updateBicyclePicture(activeBicycle);

        //piilotetaan turhat painikkeet yms.
        //lähetetään mukana
        //true, jos oma pyörä
        //false, jos ei oma pyörä
        hideInfo(ownBike);
    }

    private void updateUserInformation(Bicycle _activeBicycle){

            //----------------haetaan dataa omistajasta---------------
            DocumentReference docRef2 = db.collection("users").document(_activeBicycle.getOwner());
            docRef2.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                            //haetaan documentista tiedot kenttiihin
                            if (document.contains("address")){
                                String address = document.get("address").toString();
                                tvAddress.setText(address);
                            }

                            if (document.contains("name")){
                                String name = document.get("name").toString();
                                tvOwner.setText(name);
                            }

                            if (document.contains("phoneNumber")){
                                String phone = document.get("phoneNumber").toString();
                                tvPhone.setText(phone);
                            }
                            mProgress.dismiss();
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });
    }

    private void updateScreenFields(Bicycle _activeBicycle){

        //Päivitetään pyörän BRAND---------------------------------
        tvBrand.setText(_activeBicycle.getBrand());
        //asetetaan aktiviteetin otsikko
        getSupportActionBar().setTitle(_activeBicycle.getBrand());

        //Päivitetään pyörän MODEL---------------------------------
        tvModel.setText(_activeBicycle.getModel());

        //Päivitetään pyörän SERIAL---------------------------------
        tvSerial.setText(_activeBicycle.getSerial());

        //Päivitetään pyörän STOLEN-STATUS---------------------------------
        sStolen.setChecked(_activeBicycle.getStolen());

    }

    private void updateBicyclePicture(Bicycle _activeBicycle){

        mProgress.show();
            //Haetaan kuva palvelimelta --------------------------
            //tehdään referenssi polkupyörään hakemistoon
            StorageReference mStorageRef;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            StorageReference pathReference = mStorageRef.child("bicycles/" + _activeBicycle.getPicture());

            //haetaan kuvatiedosto palvelimelta -------------
            final long ONE_MEGABYTE = 1024 * 1024;
            pathReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    //piirretään kuva, jos kuvan haku onnistuu
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    cvPhotoOfBike.setVisibility(View.VISIBLE);
                    ivPhoto.setImageBitmap(bmp);
                    mProgress.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Log.w(TAG, "Kuvaa pyörästä ei voitu hakea! (ei löydy?!?)");
                    cvPhotoOfBike.setVisibility(View.GONE);
                    mProgress.dismiss();
                }
            });
    }

    private void hideInfo(Boolean _ownBike){

        //true, jos oma pyörä
        //false, jos ei oma pyörä
        if(_ownBike){
            //piilotetaan omistajalta hänen omat tietonsa, jos pyörä on hänen
            cvPersonalInfoBox.setVisibility(View.GONE);
        } else {
            //haetaan omistajan tiedot
            updateUserInformation(activeBicycle);
            //piilotetaan painike, jolla voi säätää pyörän varastetuksi
            cvStolenInfoBox.setVisibility(View.GONE);
        }
    }

    private void deleteBicycleButtonPressed(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        deleteCurrentPicture();
                        db.collection("bicycles").document(activeBicycle.getId())
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "Polkupyörä poistettu onnistuneesti!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Virhe polkupyörää poistettaesssa!", e);
                                    }
                                });
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Oletko varma, että haluat poistaa polkupyörän lopullisesti?").setPositiveButton("Kyllä", dialogClickListener)
                .setNegativeButton("En", dialogClickListener).show();
    }

    private void deleteCurrentPicture(){
        Log.d(TAG,"Aloitetaan kuvan poisto");
        if(activeBicycle.getPicture().equals(null) || activeBicycle.getPicture().equals("")) {
            Log.w(TAG,"Kuvatiedoston nimeä ei löytynyt!");

        } else {
            Log.d(TAG,"Kuva teidosto:" + activeBicycle.getPicture());
            StorageReference desertRef = mStorageRef.child("bicycles").child(activeBicycle.getPicture());
            Log.d(TAG,"hakemisto:" + desertRef.getPath());
            desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // File deleted successfully
                    Log.d(TAG, "Myös polkupyörän valokuva poistettiin onnistuneesti");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Uh-oh, an error occurred!
                    int errorCode = ((StorageException) exception).getErrorCode();
                    String errorMessage = exception.getMessage();
                    // test the errorCode and errorMessage, and handle accordingly
                    Log.d(TAG, "1Polkupyörän valokuvaa ei poistettu onnistuneesti, virhekoodi:" + errorMessage + " " + errorCode);
                }
            });
        }
    }

    private void mapButtonPressed(){
        //karttapainiketta on painettu
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("bicycleObject", activeBicycle);
        startActivity(intent);
    }

    private void cameraButtonPressed(){
        //tehdään kamera intentti
        Intent intent = new Intent (MediaStore.ACTION_IMAGE_CAPTURE);

        //laukaistaan intentti käyntiin
        if(intent.resolveActivity(getPackageManager()) != null){
            Log.d(TAG, "Kamera käynnistetty");
            startActivityForResult(intent,CAMERA_REQUEST_CODE);
        }
    }

    private void saveButtonPressed(){
        //TODO:tehdään aktiivisen pyörän muunnos tallennettavaan pyörään, koska muuten kaatuu?!?!?
        //TODO:syy voi olla siinä, että activeBicycle on parcelable ja parsittu kasaan?!? en tiiä.
        Bicycle bicycleToSave = new Bicycle();
        bicycleToSave.setBrand(tvBrand.getText().toString());
        bicycleToSave.setModel(tvModel.getText().toString());
        bicycleToSave.setSerial(tvSerial.getText().toString());
        bicycleToSave.setStolen(sStolen.isChecked());
        bicycleToSave.setId(activeBicycle.getId());
        bicycleToSave.setOwnerByString(activeBicycle.getOwner());
        bicycleToSave.setPicture(activeBicycle.getPicture());

        Log.d(TAG,"Aloitetaan BICYCLE objektin (ID:" + bicycleToSave.getId() + ") siirto tietokantaan..");
        //Lähetetään ja tarvittaessa ylikirjoitetaan käyttäjän tiedot
        db.collection("bicycles").document(bicycleToSave.getId()).set(bicycleToSave, SetOptions.merge());

        Log.d(TAG,"..Tietokantaan siirto onnistui");
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bicycle_menu, menu);
        if(!ownBike){
            menu.findItem(R.id.action_kartta).setVisible(false);
            menu.findItem(R.id.action_kuvaa).setVisible(false);
            menu.findItem(R.id.action_tallenna).setVisible(false);
            menu.findItem(R.id.action_poista).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //yläpalkin valinta vaihtoehdot
        int id = item.getItemId();

        if (id == R.id.action_poista){
            deleteBicycleButtonPressed();
            return true;
        } else if (id == R.id.action_kartta) {
            mapButtonPressed();
            return true;
        }  else if (id == R.id.action_tallenna){
            saveButtonPressed();
            return true;
        } else if (id == R.id.action_kuvaa){
            cameraButtonPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Aloitetaan kuvan tallennus");
        //jos aktivity resultti palauttaa kameran koodin
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Log.d(TAG, "Kamera palautti OK resultin");
            mProgress.setMessage("Tallennetaan kuvaa..");
            mProgress.show();

            //poistetaan ensin nykyinen kuva storagesta jos sellainen siellä on, ennenkuin tuhlataan enemmin muistia storagesta
            deleteCurrentPicture();

            //tallennetaan uriin data, vaikka ei tarvita, koska jos ei tallenneta, niin ohjelma kaatuu?!?
            uri = data.getData();
            Log.d(TAG, "uri haettu datasta");

            //haetaan datasta thumbnail
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            Log.d(TAG, "Pienois kuvan bittikartta haettu datasta");

            //määritetään hakemisto mihin kuva tallennetaan
            Log.d(TAG, "Tallennus id:" + activeBicycle.getId());
            StorageReference filepath = mStorageRef.child("bicycles").child(UUID.randomUUID() + ".jpg");
            Log.d(TAG, "Serverin Tiedosto polku: " + filepath);

            //kirjoitetaan sama tiedostonimi tietokantaan
            activeBicycle.setPicture(filepath.getName());
            db.collection("bicycles").document(activeBicycle.getId()).update("picture", filepath.getName());

            //muutetaan kuva bitmapista byte arrayksi, koska firestorage ei tue bitmappeja (vain uri ja bytearray)
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            imageBitmap.recycle();

            //lähetetään firestorageen kuva
            filepath.putBytes(byteArray).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //päivitetään pyörän kuva tietokannan lisäksi myös aktiiviseen ikkunaan
                    mProgress.dismiss();
                    Toast.makeText(BicycleActivity.this, "Tallennus onnistui ...", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Kuvan tallennus onnistui!");
                    updateBicyclePicture(activeBicycle);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //ilmoitetaan jos profiilikuvan päivitys epäonnistuu
                    Log.e(TAG, "Kuvan tallennus epäonnistui");
                    Toast.makeText(BicycleActivity.this, "Tallennus epäonnistui ...", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Log.e(TAG, "Kamera palautti huonon Resultin");
        }
    }

    @Override
    public void onFailure(@NonNull Exception exception) {
        // Uh-oh, an error occurred!
        int errorCode = ((StorageException) exception).getErrorCode();
        String errorMessage = exception.getMessage();
        // test the errorCode and errorMessage, and handle accordingly
        Log.d(TAG, "2Polkupyörän valokuvaa ei poistettu onnistuneesti, virhekoodi:"+ errorMessage + " " + errorCode);
    }
}
