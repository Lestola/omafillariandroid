package com.saviatsoft.omafillari;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Toast;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;

public class MainActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;
    private static final String TAG = "MARKO-MAIN-ACTIVITY";
    RecyclerView recyclerView;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<Bicycle> bicycles = new ArrayList<>();

    //latauksen etenemis indikaattori
    private ProgressDialog mProgress;
    private MenuItem plus;
    Integer maxBicycles = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);

        //yhdistetään firebaseen ja haetaan käyttäjä
        Log.d(TAG, "Aloitetaan kirjautuminen..");
        mAuth = FirebaseAuth.getInstance();
        if (mAuth != null){
        Log.d(TAG, "Firebase instanssi vastaanotettu..");
        user = mAuth.getCurrentUser();
            Log.d(TAG,"Käyttäjä " + user.getUid() + " on kirjatunut sisään.");
            //pyörien latauksen progress indikaattorin määrittely
            mProgress = new ProgressDialog(this);
            //latauksen indikaattori päälle latauksen ajaksi
            mProgress.setMessage("Haetaan polkupyöriä..");
            mProgress.show();

        } else {
            signIn();
        }
    }

    public void signIn(){
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build()
                //,new AuthUI.IdpConfig.PhoneBuilder().build()
                ,new AuthUI.IdpConfig.GoogleBuilder().build()
        );

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                user = FirebaseAuth.getInstance().getCurrentUser();
                // ...
            } else {
                //jos sign in kusi(käynnistetään uudelleen sign in)
                updateScreen();
            }
        }
    }

    public void updateScreen(){
        user = mAuth.getCurrentUser();
        if(user == null) {
            signIn();
        } else {
            getBicyclesByUser(user);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        plus = menu.findItem(R.id.action_add);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //yläpalkin valinta vaihtoehdot
        int id = item.getItemId();

        if (id == R.id.action_asetukset){
            Intent intent = new Intent(this, OptionsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_raportoi){
            Intent intent = new Intent(this, ReportActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_etsi) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }  else if (id == R.id.action_profiili){
            Intent intent = new Intent(this, UserprofileActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_add) {
            addBicycle(user);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //päivitetään näyttö joka kerta kun palataan päänäytölle (päivitetään samalla myös sen hetkinen käyttäjä)
        updateScreen();
    }

    public void addBicycle(FirebaseUser _user){
        //luodaan uusi polkupyörä objekti
        final Bicycle bicycleToAdd = new Bicycle();
        //uuden pyörän omistaja on addBicyclen mukana passattu user
        bicycleToAdd.setOwnerByUser(_user);

        db.collection("bicycles").document().set(bicycleToAdd).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "Uusi pyörä onnistuneesti lisätty");
                updateScreen();
            }

        });
    }

    public void showBicycles(List<Bicycle> foundBicycles){

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(foundBicycles, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mProgress.dismiss();
        showPlusBicycleOrNot(user);
    }

    public void getBicyclesByUser(final FirebaseUser _user){

        bicycles.clear();

        db.collection("bicycles")
                .whereEqualTo("owner", _user.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                //haetaan pyörään tiedot tietokannasta
                                Bicycle bicycle = document.toObject(Bicycle.class);
                                //heitetään mukaan documentin id numero pyörän id:ksi, koska sitä ei välttämättä ole dokumentissa tai se voi olla väärä
                                bicycle.setId(document.getId());
                                //lisätään pyörä listaan
                                bicycles.add(bicycle);
                            }
                        } else {
                            Log.i(TAG, "Error getting documents: ", task.getException());
                            Toast.makeText(MainActivity.this, "Yhtään pyörää ei löytynyt ...", Toast.LENGTH_LONG).show();
                        }
                        showBicycles(bicycles);
                    }
                });
    }

    public void showPlusBicycleOrNot(final FirebaseUser _user) {

        if(bicycles.size() < 1){
            //TODO:pyörän poiston jälkeen ei heti näytä uudestaan plussaa
            Log.d(TAG, "Ei pyöriä. Näytetään plussa");
            plus.setVisible(true);
        } else {
            Log.d(TAG, "pyöriä löytyy, piilotetaan plussa");
            plus.setVisible(false);
        }

        /* TODO:ei toimi jos puuttuu kokonaan käyttäjä (käyttäjä ei ole vielä tehnyt profiilia, siitä syystä laitetaan kiinteäksi pyörien määärä -> 1kpl
        //haetaan tietokannasta käyttäjän polkupyöriä
        DocumentReference docRef = db.collection("users").document(_user.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                if (document.contains("maxbicycles")) {
                                    maxBicycles = Integer.parseInt(document.get("maxbicycles").toString());
                                    //näytetään polkupyörän lisäyspainike, jos siihen on aihetta
                                    if(maxBicycles > bicycles.size()){
                                        //TODO:pyörän poiston jälkeen ei heti näytä uudestaan plussaa
                                        Log.d(TAG, "Käyttäjä on ostanu: " + maxBicycles + " polkupyörä paikkaa ja hänellä on " + bicycles.size() + " pyörää tallissa. Näytetään plussa");
                                        plus.setVisible(true);
                                    } else {
                                        Log.d(TAG, "Käyttäjä on ostanu: " + maxBicycles + " polkupyörä paikkaa ja hänellä on " + bicycles.size() + " pyörää tallissa. EI näytetä plussaa");
                                        plus.setVisible(false);
                                    }
                                } else {
                                    //pyörien maximimäärästä ei lötynyt tietoa
                                    Log.w(TAG, "Ei löytynyt pyörien maximimäärää");
                                }
                    }
                } else {
                    //TODO::siirrä on failureen
                    //koko käyttäjästä ei löytynyt tietoja
                    Log.e(TAG, "Virhe haettaessa käyttäjä dokumentteja: ", task.getException());
                }
            }
        });

         */
    }
}
