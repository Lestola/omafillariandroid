package com.saviatsoft.omafillari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;



public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MARKO-MAP-ACTIVITY";
    private MapView mMapView;
    private static final String MAPVIEW_BUNDLE_KEY = "AIzaSyA9juUDWys4q_asFf5rFz5Lm7RCkS7V6GQ";
    private Bicycle activeBicycle;
    private FirebaseAuth mAuth;
    FirebaseFirestore db;
    List<Report> reports = new ArrayList<>();
    RecyclerView recyclerView;
    public static GoogleMap gMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        recyclerView = findViewById(R.id.map_listOfLocations);

        //määritellään firestore
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        reports = new ArrayList<>();

        //asetetaan aktiviteetin nimi
        getSupportActionBar().setTitle("Sijainti viestit");

        //haetaan bundlesta pyörän sijainti
        activeBicycle = getIntent().getParcelableExtra("bicycleObject");

        //haetaan sijainti raportit tietokannasta
        getReportsByBicycle(activeBicycle);

        //käynnistetään google kartta
        initGoogleMap(savedInstanceState);

        //tehdään back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getReportsByBicycle(final Bicycle _bicycle){
        Log.d(TAG,"Aloitetaan raporttien haku tietokannasta");

        reports.clear();
        //.orderBy("time", Direction.DESCENDING);

        db.collection("reports")
                .whereEqualTo("serial", _bicycle.getSerial()).orderBy("time", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                //haetaan raportin tiedot tietokannasta
                                Report report = document.toObject(Report.class);

                                Log.d(TAG, "muutto onnistui objekti muotoon");
                                //lisätään pyörä listaan
                                reports.add(report);
                            }
                        } else {
                            Log.i(TAG, "Error getting documents: ", task.getException());
                            Toast.makeText(MapActivity.this, "Yhtään viestiä ei löytynyt ...", Toast.LENGTH_LONG).show();
                        }
                        Log.d(TAG,"Aloitetaan populointi");
                        populateListView(reports);
                    }
                });
    }

    private void populateListView(List<Report> _reports){
        Log.d(TAG,"populointi aloitettu");

        ReportListAdapter adapter = new ReportListAdapter(_reports,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Log.d(TAG, "adapterointi valmis");
    }


    private void initGoogleMap(Bundle savedInstanceState){
        //käynnistetään kartta
        Bundle mapViewBundle = null;
        if(savedInstanceState != null){
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = findViewById(R.id.map_mvMap);
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync( this);
    }

    @Override
    public void onMapReady(GoogleMap _map){
        //annetaan gMapille valmis kartta.. tai jotaa
        //tähän voi lisätä sitten myöhemmin, jos kartan pitää heti alussa zoomata johinkin tai jotain.
        gMap = _map;

    }

    @Override
    public void onStart(){
        mMapView.onStart();
        super.onStart();;
    }

    @Override
    public void onResume(){
        mMapView.onResume();
        super.onResume();;
    }

    @Override
    public void onStop(){
        mMapView.onStop();
        super.onStop();;
    }

    @Override
    public void onPause(){
        mMapView.onPause();
        super.onPause();;
    }

    @Override
    public void onDestroy(){
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mMapView.onLowMemory();;
    }


}


