package com.saviatsoft.omafillari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class OptionsActivity extends AppCompatActivity {

    private static final String TAG = "MARKO-OPTIONS-ACTIVITY";
    Button btnSignOut;
    CheckBox cbHideProfileFields;
    Switch sName, sAddress, sPhone, sEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        //viewejä.. mitä siitä sen enempää
        /*
        cbHideProfileFields = findViewById(R.id.cbHide);
        sAddress = findViewById(R.id.sShowAddress);
        sEmail = findViewById(R.id.sShowEmail);
        sName = findViewById(R.id.sShowName);
        sPhone = findViewById(R.id.sShowPhoneNumber);
        */

        btnSignOut = findViewById(R.id.options_logout);

        //asetetaan aktiviteetin nimi
        getSupportActionBar().setTitle("Asetukset");

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                signOut();
            }
        });

        updateSwitchThings();
        /*
        cbHideProfileFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSwitchThings();
            }
        });

         */

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void signOut(){
        //kirjaudutaan ulos firebase tililtä
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        finish();
                    }
                });
    }

    public void updateSwitchThings(){
        /*
        //jos raksi ruutuun, niin käyttöön liu'ut
         if(cbHideProfileFields.isChecked()){
            sAddress.setEnabled(true);
            sName.setEnabled(true);
            sPhone.setEnabled(true);
            sEmail.setEnabled(true);
        } else {
            sAddress.setEnabled(false);
            sName.setEnabled(false);
            sPhone.setEnabled(false);
            sEmail.setEnabled(false);
        }

         */
    }

}
