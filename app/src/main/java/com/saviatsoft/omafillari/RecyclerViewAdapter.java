package com.saviatsoft.omafillari;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "MARKO-RECYCLER-ADAPTER";

    private List<Bicycle> mUserBicycles;
    private Context mContext;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private StorageReference mStorageRef;

    private FirebaseUser mCurrentUser = mAuth.getCurrentUser();

    public RecyclerViewAdapter(List<Bicycle> userBicycles, Context context){
        mUserBicycles = userBicycles;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,parent,false);
        ViewHolder holder = new ViewHolder(view);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        //haetaan brandi----------
        holder.bicycleBrand.setText(mUserBicycles.get(position).getBrand());

        //haetaan kuva-----------
        if (mUserBicycles.get(position).getPicture() != ""){
            String bicyclePictureFilename = mUserBicycles.get(position).getPicture();
            //tehdään referenssi polkupyörään hakemistoon
            StorageReference pathReference = mStorageRef.child("bicycles/" + bicyclePictureFilename);

            final long ONE_MEGABYTE = 1024 * 1024;
            pathReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    //piirretään kuva, jos kuvan haku onnistuu
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.bicycleImage.setImageBitmap(bmp);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });
        }

        //haetaan malli------------
        holder.bicycleModel.setText(mUserBicycles.get(position).getModel());

        //haetaan varastamis status ------------
        if(mUserBicycles.get(position).getStolen()){
            holder.parentLayout.setBackgroundColor(0xFFFF6363);
        }

        //haetaan sarjanumero ----------------
        holder.bicycleSerial.setText(mUserBicycles.get(position).getSerial());

        //tässä vaiheessa tarkastetaan onko haun tekijä pyörän omistaja
        final boolean mBicycleOwn;

        Log.d(TAG, "Olion owner:" + mUserBicycles.get(position).getOwner() + ":::Tämänhetkinen UID:" + mCurrentUser.getUid());
        if(mUserBicycles.get(position).getOwner().equals(mCurrentUser.getUid())){
            mBicycleOwn = true;
            Log.d(TAG, "Omapyörä:TRUE");
        } else{
            mBicycleOwn = false;
            Log.d(TAG, "Omapyörä:FALSE");
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, mBrands.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext.getApplicationContext(), BicycleActivity.class);
                intent.putExtra("ownBike", mBicycleOwn);
                //intent.putExtra("bicycleObject", mUserBicycles.get(position).getId());
                //intent.putExtra("bicycleIDnumber", mUserBicycles.get(position).getId());
                intent.putExtra("bicycleObject", mUserBicycles.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserBicycles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView bicycleImage;
        TextView bicycleBrand;
        TextView bicycleModel;
        TextView bicycleSerial;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            bicycleImage = itemView.findViewById(R.id.liImage);
            bicycleBrand = itemView.findViewById(R.id.liBrand);
            bicycleModel = itemView.findViewById(R.id.liModel);
            bicycleSerial = itemView.findViewById(R.id.liSerial);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
