package com.saviatsoft.omafillari;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

public class Report {

    private String message, user, serial, photo;
    public GeoPoint location;
    public Timestamp time;

    public Report(){
        this.message = "";
        this.serial = "";
        this.photo = "";
        this.time = Timestamp.now();
        this.user = "";
        this.location = new GeoPoint(0.0,0.0);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getMessage() {
        return message;
    }

    public String getPhoto() {
        return photo;
    }

    public String getSerial() {
        return serial;
    }

    public String getUser() {
        return user;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public Timestamp getTime() {
        return time;
    }
}
