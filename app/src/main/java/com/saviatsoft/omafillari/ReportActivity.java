package com.saviatsoft.omafillari;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

public class ReportActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MARKO-REPORT-ACTIVITY";
    private MapView mMapView;
    private static final String MAPVIEW_BUNDLE_KEY = "AIzaSyA9juUDWys4q_asFf5rFz5Lm7RCkS7V6GQ";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    CheckBox cbAddLocation;
    Button btnSendReport;
    EditText edSerial, edMessage;
    Boolean addLocation = false;
    FirebaseFirestore db;
    FirebaseAuth mAuth;

    //käynnistetään sijainnin kyttäys

    Location location;
    LocationManager locationManager;
    //private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        cbAddLocation = findViewById(R.id.report_addLocationCheckBox);
        btnSendReport = findViewById(R.id.action_reportActivity_search);
        edSerial = findViewById(R.id.report_SerialNumber);
        edMessage = findViewById(R.id.report_message);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.

            ActivityCompat.requestPermissions(ReportActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

            return;
        }
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        //käynnistetään google kartta
        initGoogleMap(savedInstanceState);

        //kuunnellaan, jos paikan tallennus valintaruudun tila muuttuu
        cbAddLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (buttonView.isChecked()) {
                    // checked
                    addLocation = true;
                }
                else
                {
                    // not checked
                    addLocation = false;
                }
            }
        });

        btnSendReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendReportButtonClicked();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    addLocation = true;
                    cbAddLocation.setEnabled(true);
                    cbAddLocation.setChecked(true);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    addLocation = false;
                    cbAddLocation.setEnabled(false);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void SendReportButtonClicked(){
        //nykyinen sijainti
        GeoPoint firebaseLocation = new GeoPoint(location.getLatitude(), location.getLongitude());

        //Luodaan palvelimelle lähetettävä objekti, mikä sisältää pyörän tiedot
        Report report = new Report();
        report.setUser(mAuth.getCurrentUser().getUid());
        report.setSerial(edSerial.getText().toString());
        report.setMessage(edMessage.getText().toString());
        report.setLocation(firebaseLocation);

        //lähetetään raportti objekti palvelimelle.
        db.collection("reports").document().set(report).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "Raportti lähetetty onnistuneesti");
                Toast.makeText(ReportActivity.this, "Ilmoitus tallennettu järjestelmään!", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    private void initGoogleMap(Bundle savedInstanceState){
        //käynnistetään kartta
        Bundle mapViewBundle = null;
        if(savedInstanceState != null){
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = findViewById(R.id.report_mapScreen);
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync( this);
    }

    @Override
    public void onMapReady(GoogleMap _map){

        GoogleMap gMap = _map;

        LatLng currentLocation = new LatLng(location.getLatitude(),location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentLocation);
        markerOptions.title("Nykyinen sijaintisi");
        float zoom = gMap.getMaxZoomLevel() - 8 ;

        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, zoom));
        gMap.addMarker(markerOptions);
    }

    @Override
    public void onStart(){
        mMapView.onStart();
        super.onStart();;
    }

    @Override
    public void onResume(){
        mMapView.onResume();
        super.onResume();;
    }

    @Override
    public void onStop(){
        mMapView.onStop();
        super.onStop();;
    }

    @Override
    public void onPause(){
        mMapView.onPause();
        super.onPause();;
    }

    @Override
    public void onDestroy(){
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mMapView.onLowMemory();;
    }



}
