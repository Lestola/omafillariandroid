package com.saviatsoft.omafillari;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ViewHolder> {
    private static final String TAG = "MARKO-REPORT-ADAPTER";

    private List<Report> mReports;
    private Context mContext;

    public ReportListAdapter(List<Report> reports, Context context) {
        mReports = reports;
        mContext = context;
        Log.d(TAG,"Contexti asetettu");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_maplistitem,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBincViewHolder : called." + position);

        holder.tvMessage.setText(mReports.get(position).getMessage());
        holder.tvTimeStamp.setText(mReports.get(position).getTime().toDate().toString());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapActivity.gMap.clear();
                LatLng bikeLocation = new LatLng(mReports.get(position).getLocation().getLatitude(), mReports.get(position).getLocation().getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(bikeLocation);
                markerOptions.title(mReports.get(position).getMessage());
                float zoom = MapActivity.gMap.getMaxZoomLevel() - 8 ;
                MapActivity.gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(bikeLocation, zoom));
                MapActivity.gMap.addMarker(markerOptions);
            }
        });
    }

    @Override
    public int getItemCount() { return mReports.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvMessage;
        TextView tvTimeStamp;
        LinearLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMessage = itemView.findViewById(R.id.mapObject_message);
            tvTimeStamp = itemView.findViewById(R.id.mapObject_timeStamp);
            parentLayout = itemView.findViewById(R.id.map_parent_layout);

        }
    }
}
