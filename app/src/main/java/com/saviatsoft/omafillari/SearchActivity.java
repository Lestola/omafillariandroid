package com.saviatsoft.omafillari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

       private static final String TAG = "MARKO-SEARCH-ACTIVITY";

       Button btnSearch;
       EditText etSerial;

       private List<Bicycle> bicycles = new ArrayList<>();

       private FirebaseFirestore db = FirebaseFirestore.getInstance();

    //latauksen etenemis indikaattori
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //pyörien latauksen progress indikaattorin määrittely
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Haetaan polkupyöriä..");

        //tehdään back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //asetetaan aktiviteetin nimi
        getSupportActionBar().setTitle("Etsi runkonumerolla");

        //määritellään painikkeet
        btnSearch = findViewById(R.id.bMagnifiyingGlass);
        etSerial = findViewById(R.id.etSearchBox);



        //kun etsintä painiketta painetaan
        btnSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "Save Button Pressed");
                //siirrytään tallentamaan käyttäjän tietoja
                searchButtonPressed();
            }
        });

        etSerial.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    btnSearch.performClick();
                }
                return false;
            }
        });
    }

    private void searchButtonPressed(){

        //näytetään progressbar
        mProgress.show();

        //käsketään näppäimistö pois tieltä
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etSerial.getWindowToken(), 0);

        //haetaan lista pyöristä ja passateaan se showBicycles funktiolle.
        getBicyclesBySerial(etSerial.getText().toString());

    }

    public void getBicyclesBySerial(String _serial){
        bicycles.clear();
        //haetaan tietokannasta käyttäjän polkupyöriä
        db.collection("bicycles").whereEqualTo("serial", _serial).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                Bicycle bicycle = new Bicycle();

                                if (document.contains("brand")){
                                    String brand = document.get("brand").toString();
                                    bicycle.setBrand(brand);
                                }

                                if (document.contains("lastlocation")){
                                    GeoPoint lastlocation = document.getGeoPoint("lastlocation");
                                }

                                if (document.contains("model")){
                                    String model = document.get("model").toString();
                                    bicycle.setModel(model);
                                }

                                if (document.contains("owner")){
                                    String owner = document.get("owner").toString();
                                    bicycle.setOwnerByString(owner);
                                }
                                if (document.contains("picture")){
                                    String picture = document.get("picture").toString();
                                    bicycle.setPicture(picture);
                                }
                                if (document.contains("serial")){
                                    String serial = document.get("serial").toString();
                                    bicycle.setSerial(serial);
                                }
                                if (document.contains("stolen")){
                                    Boolean stolen = Boolean.parseBoolean(document.get("owner").toString());
                                    bicycle.setStolen(stolen);
                                }

                                bicycle.setId(document.getId());

                                //lisätään pyörä listaan
                                bicycles.add(bicycle);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        showBicycles(bicycles);
                    }
                });
    }

    public void showBicycles(List<Bicycle> foundBicycles){

        RecyclerView recyclerView = findViewById(R.id.rvResultBox);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(foundBicycles, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mProgress.dismiss();
    }
}
