package com.saviatsoft.omafillari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.HashMap;

public class UserprofileActivity extends AppCompatActivity {

    private static final String TAG = "MARKO-PROFILE-ACTIVITY";

    //Layout itemit
    private TextView tvAddress, tvName, tvPhone, tvEmail;
    private ImageView ivProfilePicture;
    Intent CropIntent;
    Uri uri;

    //Firebase tavaraa
    StorageReference mStorageRef;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    //kameran pyyntö koodi
    private static final int CAMERA_REQUEST_CODE = 1;

    //latauksen etenemis indikaattori
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);

        //asetetaan aktiviteetin nimi
        getSupportActionBar().setTitle("Profiili");

        //linkitetään kentät
        tvAddress = findViewById(R.id.tvAddress);
        tvEmail = findViewById(R.id.tvEmail);
        tvName = findViewById(R.id.tvName);
        tvPhone = findViewById(R.id.tvPhone);

        //profiilikuvan linkitys
        ivProfilePicture = findViewById(R.id.ivProfilePicture);

        //kuva latauksen progress indikaattorin määrittely
        mProgress = new ProgressDialog(this);

        //haetaan nykyinen käyttäjä
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        //haetaan käyttäjän tiedot tietokannasta
        getUserData(currentUser);


    }

    private void getUserData(final FirebaseUser currentUser){

        mProgress.setMessage("Haetaan käyttäjätietoja..");
        mProgress.show();

        //haetaan authista suoraan sähköpostiosoite.
        tvEmail.setText(mAuth.getCurrentUser().getEmail());

        //edetään, jos käyttäjä on kirjautunut sisään
        if(currentUser != null){
            //----------------haetaan dataa----------------
            DocumentReference docRef = db.collection("users").document(mAuth.getUid());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                            //haetaan documentista tiedot kenttiihin
                            if (document.contains("name")){
                                String name = document.get("name").toString();
                                tvName.setText(name);
                            }

                            if (document.contains("address")){
                                String address = document.get("address").toString();
                                tvAddress.setText(address);
                            }

                            if (document.contains("phoneNumber")){
                                String phoneNumber = document.get("phoneNumber").toString();
                                tvPhone.setText(phoneNumber);
                            }

                            //päivitetään profiilikuva
                            updateUserProfilePicture(currentUser);

                            mProgress.dismiss();
                        } else {
                            Log.d(TAG, "Ei löytynyt dokumenttia");
                            mProgress.dismiss();
                            Toast.makeText(UserprofileActivity.this, "Käyttäjätietoja ei löytynyt...", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "Ongelma käyttäjän haussa!", task.getException());
                        mProgress.dismiss();
                        Toast.makeText(UserprofileActivity.this, "Yhteysongelma...", Toast.LENGTH_LONG).show();
                    }
                }
            });
            //--------------------------------
        }

    }

    private void updateUserProfilePicture(final FirebaseUser currentUser){

        //Luodaan referenssi tallennushakemistoon, josta profiili kuva haetaan
        String profilePictureFilename = currentUser.getUid();
        StorageReference filepath = mStorageRef.child("users/" + profilePictureFilename);

        //haetaan kuvatiedosto palvelimelta -------------
        final long ONE_MEGABYTE = 1024 * 1024;
        filepath.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                //piirretään kuva, jos kuvan haku onnistuu
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                ivProfilePicture.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Jos homma menee käteen
                Toast.makeText(UserprofileActivity.this, "Profiilikuvaa ei löytynyt ...", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //luodaan ylämenu
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //mitä ylämenun itemiä on klikattu
        int id = item.getItemId();

        //suoritetaan ylämenun toiminto
        if (id == R.id.action_checkmark){
            saveUserData(currentUser);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveUserData(FirebaseUser currentUser){

        //napataan tekstit, mitä kenttiihin on kirjoitettu
        String address = tvAddress.getText().toString();
        String phoneNumber = tvPhone.getText().toString();
        String name = tvName.getText().toString();

        //Luodaan palvelimelle lähetettävä objekti, mikä sisältää käyttäjän tiedot
        Map<String, Object> user = new HashMap<>();
        user.put("name", name);
        user.put("address", address);
        user.put("phoneNumber", phoneNumber);
        user.put("photo", currentUser.getUid());
        user.put( "email", mAuth.getCurrentUser().getEmail());

        //Lähetetään ja tarvittaessa ylikirjoitetaan käyttäjän tiedot
        db.collection("users").document(mAuth.getUid()).set(user, SetOptions.merge());
    }

    public void takePicture(View view) {
        //tehdään kamera intentti
        Intent intent = new Intent (MediaStore.ACTION_IMAGE_CAPTURE);

        //laukaistaan intentti käyntiin
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent,CAMERA_REQUEST_CODE);
        }
    }

    // imagen croppaus-------------------------
    /*
    private void cropImage(){

        try{
            CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 180);
            CropIntent.putExtra("outputY", 180);
            CropIntent.putExtra("aspectX", 3);
            CropIntent.putExtra("aspectY", 4);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent,1);
        } catch (ActivityNotFoundException ex){

        }
    }
    */


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //jos aktivity resultti palauttaa kameran koodin
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            mProgress.setMessage("Tallennetaan kuvaa..");
            mProgress.show();

            //tallennetaan uriin data, vaikka ei tarvita, koska jos ei tallenneta, niin ohjelma kaatuu?!?
            uri = data.getData();
            //cropImage();

            //haetaan datasta thumbnail
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            //määritetään hakemisto mihin kuva tallennetaan
            StorageReference filepath = mStorageRef.child("users").child(currentUser.getUid());

            //muutetaan kuva bitmapista byte arrayksi, koska firestorage ei tue bitmappeja (vain uri ja bytearray)
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            imageBitmap.recycle();

            //lähetetään firestorageen kuva
            filepath.putBytes(byteArray).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //päivitetään profiilikuva
                    updateUserProfilePicture(currentUser);
                    mProgress.dismiss();
                    Toast.makeText(UserprofileActivity.this, "Tallennus onnistui ...", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //ilmoitetaan jos profiilikuvan päivitys epäonnistuu
                    Toast.makeText(UserprofileActivity.this, "Tallennus epäonnistui ...", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
